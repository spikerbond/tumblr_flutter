package alexislarson.tumblrflutter;

import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.google.gson.GsonBuilder;
import com.tumblr.jumblr.JumblrClient;
import com.tumblr.jumblr.types.AnswerPost;
import com.tumblr.jumblr.types.AudioPost;
import com.tumblr.jumblr.types.ChatPost;
import com.tumblr.jumblr.types.LinkPost;
import com.tumblr.jumblr.types.Note;
import com.tumblr.jumblr.types.Photo;
import com.tumblr.jumblr.types.PhotoPost;
import com.tumblr.jumblr.types.Post;
import com.tumblr.jumblr.types.QuotePost;
import com.tumblr.jumblr.types.TextPost;
import com.tumblr.jumblr.types.UnknownTypePost;
import com.tumblr.jumblr.types.Video;
import com.tumblr.jumblr.types.VideoPost;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

//import java.util.LinkedHashMap;
//import java.util.LinkedHashMap;

class Tumblr{

	JumblrClient client;

	private HashMap<String, List> posts = new HashMap<>();
	private List<Post> postList;

	//private boolean gotPosts = false;
	private Long lastPostID;// = 173925801713l;
	private Long lastPostTimestamp;
	Long morePostsID;
	private boolean first_run = true;
	String postType;

	HashMap<String, ?> getPosts(HashMap<String, ?> args){
		Gson gson = new GsonBuilder()
				.create();
		// we ignore Private fields
		//builder.excludeFieldsWithModifiers(Modifier.PRIVATE);
		String json = "";

		client = new JumblrClient(SecureDataStatic.getConsumerKey(), SecureDataStatic.getConsumerSecret(), SecureDataStatic.getUserToken(), SecureDataStatic.getUserKey());
		//client.xauth("spikerbond@gmail.com", "0YOuSIUBB4UvppJn4EZCQDNav3VogY");
		try{
			postList = new GetPosts(this, args).execute().get(20L, TimeUnit.SECONDS);
			//Log.wtf("postlist", postList.toString());
			//Log.wtf("postlist", postList.toArray().toString().toString());
			json = gson.toJson(postList, new TypeToken<List<Post>>() {}.getType());

			posts.put("posts", postListToHash(postList));
		}catch(Exception e){
			Log.wtf("get posts error", e.getMessage());
			e.printStackTrace();
		}
		//Log.wtf("Posts map to string",posts.toString());

		// serialize the albums object

		//Log.wtf("gson", json);


		try{
			new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "GsonOut.txt").createNewFile();
			Log.wtf("output file","created file in downloads?");
			File output = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "GsonOut.txt");
			FileOutputStream stream = new FileOutputStream(output);
			Log.wtf("output file","created stream");
			try{
				Log.wtf("output file","about to write to file");
				stream.write(json.getBytes());
			}finally{
				Log.wtf("output file","output to file");
				stream.close();
			}
		}catch(Exception e){
			e.printStackTrace();
			Log.wtf("file errror", e.getStackTrace().toString());
		}

		return posts;
	}

	public File getPublicAlbumStorageDir(String filename){
		// Get the directory for the user's public pictures directory.
		File file = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DOWNLOADS), filename);
		if(!file.mkdirs()){
			Log.e("save to file", "Directory not created");
		}
		return file;
	}

	private List<Map> postListToHash(List<Post> postsList){
		//LinkedLinkedHashMap<String, Object> temp_post_list = new LinkedLinkedHashMap<>();
		List<Map> temp_post_list = new ArrayList<>();

		//Log.wtf("tumblr get posts", "post list length: " + postsList.size());

		for(Post post : postsList){
			HashMap<String, Object> postMap = new HashMap<>();

			postMap.put("type", post.getType().getValue());
			postMap.put("authorId", post.getAuthorId());
			postMap.put("blogName", post.getBlogName());
			postMap.put("dateGMT", post.getDateGMT());
			postMap.put("format", post.getFormat());
			postMap.put("id", post.getId());
			postMap.put("likedTimestamp", post.getLikedTimestamp());
			postMap.put("postUrl", post.getPostUrl());
			postMap.put("rebloggedFromId", post.getRebloggedFromId());
			postMap.put("rebloggedFromName", post.getRebloggedFromName());
			postMap.put("rebloggedFromTitle", post.getRebloggedFromTitle());
			postMap.put("rebloggedFromUrl", post.getRebloggedFromUrl());
			postMap.put("rebloggedRootId", post.getRebloggedRootId());
			postMap.put("rebloggedRootName", post.getRebloggedRootName());
			postMap.put("rebloggedRootTitle", post.getRebloggedRootTitle());
			postMap.put("rebloggedRootUrl", post.getRebloggedRootUrl());
			postMap.put("reblogkey", post.getReblogKey());
			postMap.put("shortUrl", post.getShortUrl());
			postMap.put("slug", post.getSlug());
			postMap.put("sourceTitle", post.getSourceTitle());
			postMap.put("sourceUrl", post.getSourceUrl());
			postMap.put("state", post.getState());
			postMap.put("timestamp", post.getTimestamp());
			postMap.put("noteCount", post.getNoteCount());

			//Log.wtf("note count", "there are " + post.getNoteCount());
			if(post.getNotes() != null){
				//Log.wtf("notes size", "there are " + post.getNotes().size());
				List<Map> temp_notes_map = new ArrayList<>();
				for(Note tempNote : post.getNotes()){
					HashMap<String, Object> temp_map = new HashMap<>();
					temp_map.put("addedText", tempNote.getAddedText());
					temp_map.put("blogName", tempNote.getBlogName());
					temp_map.put("blogUrl", tempNote.getBlogUrl());
					temp_map.put("postID", tempNote.getPostId());
					temp_map.put("replyText", tempNote.getReplyText());
					temp_map.put("timestamp", tempNote.getTimestamp());
					temp_map.put("type", tempNote.getType());

					temp_notes_map.add(temp_map);
				}
				postMap.put("notes", temp_notes_map);
			}
			if(post.getTags() != null)
				postMap.put("tags", post.getTags());


			switch(postMap.get("type").toString()){
				case "answer":{
					AnswerPost temp = (AnswerPost) post;
					postMap.put("answer", temp.getAnswer());
					postMap.put("askingName", temp.getAskingName());
					postMap.put("askingUrl", temp.getAskingUrl());
					postMap.put("question", temp.getQuestion());
					break;
				}
				case "audio":{
					AudioPost temp = (AudioPost) post;
					postMap.put("albumArtUrl", temp.getAlbumArtUrl());
					postMap.put("albumName", temp.getAlbumName());
					postMap.put("artistName", temp.getArtistName());
					postMap.put("audioUrl", temp.getAudioUrl());
					postMap.put("caption", temp.getCaption());
					postMap.put("auidoPlayer", temp.getEmbedCode());
					postMap.put("playCount", temp.getPlayCount());
					postMap.put("trackName", temp.getTrackName());
					postMap.put("trackNumber", temp.getTrackNumber());
					postMap.put("year", temp.getYear());
					break;
				}
				case "chat":{
					ChatPost temp = (ChatPost) post;
					postMap.put("body", temp.getBody());
					postMap.put("dialogue", temp.getDialogue());
					postMap.put("title", temp.getTitle());
					break;
				}
				case "link":{
					LinkPost temp = (LinkPost) post;
					postMap.put("description", temp.getDescription());
					postMap.put("linkUrl", temp.getLinkUrl());
					postMap.put("title", temp.getTitle());
					break;
				}
				case "photo":{
					PhotoPost temp = (PhotoPost) post;
					postMap.put("caption", temp.getCaption());
					postMap.put("height", temp.getHeight());
					postMap.put("width", temp.getWidth());

					Iterator<Photo> photoIterator = temp.getPhotos().iterator();
					int i = 0;
					List<Map> temp_photo_map = new ArrayList<>();
					while(photoIterator.hasNext()){
						Photo tempPhoto = photoIterator.next();
						HashMap<String, Object> temp_map = new HashMap<>();
						temp_map.put("caption", tempPhoto.getCaption());
						temp_map.put("originalHeight", tempPhoto.getOriginalSize().getHeight());
						temp_map.put("originalUrl", tempPhoto.getOriginalSize().getUrl());
						temp_map.put("originalWidth", tempPhoto.getOriginalSize().getWidth());

						temp_photo_map.add(temp_map);
						i++;
					}
					postMap.put("photos", temp_photo_map);

					break;
				}
				case "quote":{
					QuotePost temp = (QuotePost) post;
					postMap.put("source", temp.getSource());
					postMap.put("text", temp.getText());
					break;
				}
				case "text":{
					TextPost temp = (TextPost) post;
					postMap.put("body", temp.getBody());
					postMap.put("title", temp.getTitle());
					break;
				}
				case "video":{
					VideoPost temp = (VideoPost) post;
					postMap.put("caption", temp.getCaption());
					postMap.put("permaLinkUrl", temp.getPermalinkUrl());
					postMap.put("thumbnailHeight", temp.getThumbnailHeight());
					postMap.put("thumbnailUrl", temp.getThumbnailUrl());
					postMap.put("thumbnailWidth", temp.getThumbnailWidth());

					Iterator<Video> videoIterator = temp.getVideos().iterator();
					int i = 0;
					List<Map> temp_video_map = new ArrayList<>();
					while(videoIterator.hasNext()){
						Video tempVideo = videoIterator.next();
						HashMap<String, Object> temp_map = new HashMap<>();
						temp_map.put("videoPlayer", tempVideo.getEmbedCode());
						temp_map.put("width", tempVideo.getWidth());

						temp_video_map.add(temp_map);
						i++;
					}
					postMap.put("videos", temp_video_map);

					break;
				}
				default:{ //Unknown type
					break;

				}
			}

			temp_post_list.add(postMap);
		}
		Post temp = postsList.get(postList.size() - 1);
		lastPostID = temp.getId();
		lastPostTimestamp = temp.getTimestamp();
		//Log.wtf("tumblr get posts", "last post id = " + lastPostID.toString());
		return temp_post_list;
	}

/*

	private LinkedHashMap<String, ?> postListToHash(List<Post> postsList){
		LinkedLinkedHashMap<String, Object> temp_post_list = new LinkedLinkedHashMap<>();

		//Log.wtf("tumblr get posts", "post list length: " + postsList.size());

		for(Post post : postsList){
			LinkedHashMap<String, Object> postMap = new LinkedHashMap<>();

			postMap.put("type", post.getType().getValue());
			postMap.put("authorId", post.getAuthorId());
			postMap.put("blogName", post.getBlogName());
			postMap.put("dateGMT", post.getDateGMT());
			postMap.put("format", post.getFormat());
			postMap.put("id", post.getId());
			postMap.put("likedTimestamp", post.getLikedTimestamp());
			postMap.put("postUrl", post.getPostUrl());
			postMap.put("rebloggedFromId", post.getRebloggedFromId());
			postMap.put("rebloggedFromName", post.getRebloggedFromName());
			postMap.put("rebloggedFromTitle", post.getRebloggedFromTitle());
			postMap.put("rebloggedFromUrl", post.getRebloggedFromUrl());
			postMap.put("rebloggedRootId", post.getRebloggedRootId());
			postMap.put("rebloggedRootName", post.getRebloggedRootName());
			postMap.put("rebloggedRootTitle", post.getRebloggedRootTitle());
			postMap.put("rebloggedRootUrl", post.getRebloggedRootUrl());
			postMap.put("reblogkey", post.getReblogKey());
			postMap.put("shortUrl", post.getShortUrl());
			postMap.put("slug", post.getSlug());
			postMap.put("sourceTitle", post.getSourceTitle());
			postMap.put("sourceUrl", post.getSourceUrl());
			postMap.put("state", post.getState());
			postMap.put("timestamp", post.getTimestamp());
			postMap.put("noteCount", post.getNoteCount());

			//Log.wtf("note count", "there are " + post.getNoteCount());
			int i = 0;
			if(post.getNotes() != null){
				//Log.wtf("notes size", "there are " + post.getNotes().size());
				LinkedHashMap<String, Object> temp_notes_map = new LinkedHashMap<>();
				for(Note tempNote : post.getNotes()){
					LinkedHashMap<String, Object> temp_map = new LinkedHashMap<>();
					temp_map.put("addedText", tempNote.getAddedText());
					temp_map.put("blogName", tempNote.getBlogName());
					temp_map.put("blogUrl", tempNote.getBlogUrl());
					temp_map.put("postID", tempNote.getPostId());
					temp_map.put("replyText", tempNote.getReplyText());
					temp_map.put("timestamp", tempNote.getTimestamp());
					temp_map.put("type", tempNote.getType());

					temp_notes_map.put("note " + i, temp_map);
					i++;
				}
				postMap.put("notes", temp_notes_map);
			}

			if(post.getTags() != null){
				Iterator<String> tagIterator = post.getTags().iterator();
				i = 0;
				LinkedHashMap<String, Object> temp_tag_map = new LinkedHashMap<>();
				while(tagIterator.hasNext()){
					temp_tag_map.put("tag " + i, tagIterator.next());
					i++;
				}
				postMap.put("tags", temp_tag_map);
			}


			switch(postMap.get("type").toString()){
				case "answer":{
					AnswerPost temp = (AnswerPost) post;
					postMap.put("answer", temp.getAnswer());
					postMap.put("askingName", temp.getAskingName());
					postMap.put("askingUrl", temp.getAskingUrl());
					postMap.put("question", temp.getQuestion());
					break;
				}
				case "audio":{
					AudioPost temp = (AudioPost) post;
					postMap.put("albumArtUrl", temp.getAlbumArtUrl());
					postMap.put("albumName", temp.getAlbumName());
					postMap.put("artistName", temp.getArtistName());
					postMap.put("audioUrl", temp.getAudioUrl());
					postMap.put("caption", temp.getCaption());
					postMap.put("embedCode", temp.getEmbedCode());
					postMap.put("playCount", temp.getPlayCount());
					postMap.put("trackName", temp.getTrackName());
					postMap.put("trackNumber", temp.getTrackNumber());
					postMap.put("year", temp.getYear());
					break;
				}
				case "chat":{
					ChatPost temp = (ChatPost) post;
					postMap.put("body", temp.getBody());
					postMap.put("dialog", temp.getDialogue());
					postMap.put("title", temp.getTitle());
					break;
				}
				case "link":{
					LinkPost temp = (LinkPost) post;
					postMap.put("description", temp.getDescription());
					postMap.put("linkUrl", temp.getLinkUrl());
					postMap.put("title", temp.getTitle());
					break;
				}
				case "photo":{
					PhotoPost temp = (PhotoPost) post;
					postMap.put("caption", temp.getCaption());
					postMap.put("height", temp.getHeight());
					postMap.put("width", temp.getWidth());

					Iterator<Photo> photoIterator = temp.getPhotos().iterator();
					i = 0;
					LinkedHashMap<String, Object> temp_photo_map = new LinkedHashMap<>();
					while(photoIterator.hasNext()){
						Photo tempPhoto = photoIterator.next();
						LinkedHashMap<String, Object> temp_map = new LinkedHashMap<>();
						temp_map.put("caption", tempPhoto.getCaption());
						temp_map.put("originalHeight", tempPhoto.getOriginalSize().getHeight());
						temp_map.put("originalUrl", tempPhoto.getOriginalSize().getUrl());
						temp_map.put("originalWidth", tempPhoto.getOriginalSize().getWidth());

						temp_photo_map.put("photo " + i, temp_map);
						i++;
					}
					postMap.put("photos", temp_photo_map);

					break;
				}
				case "quote":{
					QuotePost temp = (QuotePost) post;
					postMap.put("source", temp.getSource());
					postMap.put("text", temp.getText());
					break;
				}
				case "text":{
					TextPost temp = (TextPost) post;
					postMap.put("body", temp.getBody());
					postMap.put("title", temp.getTitle());
					break;
				}
				case "video":{
					VideoPost temp = (VideoPost) post;
					postMap.put("caption", temp.getCaption());
					postMap.put("permaLinkUrl", temp.getPermalinkUrl());
					postMap.put("thumbnailHeight", temp.getThumbnailHeight());
					postMap.put("thumbnailUrl", temp.getThumbnailUrl());
					postMap.put("thumbnailWidth", temp.getThumbnailWidth());

					Iterator<Video> videoIterator = temp.getVideos().iterator();
					i = 0;
					LinkedHashMap<String, Object> temp_video_map = new LinkedHashMap<>();
					while(videoIterator.hasNext()){
						Video tempVideo = videoIterator.next();
						LinkedHashMap<String, Object> temp_map = new LinkedHashMap<>();
						temp_map.put("embedCode", tempVideo.getEmbedCode());
						temp_map.put("width", tempVideo.getWidth());

						temp_video_map.put("video " + i, temp_map);
						i++;
					}
					postMap.put("videos", temp_video_map);

					break;
				}
				default:{ //Unknown type
					break;

				}
			}

			temp_post_list.put(post.getId().toString(), postMap);
		}
		Post temp = postsList.get(postList.size() - 1);
		lastPostID = temp.getId();
		lastPostTimestamp = temp.getTimestamp();
		Log.wtf("tumblr get posts", "last post id = " + lastPostID.toString());
		return temp_post_list;
	}

*/


	/*
	void createClient(String consumer_key, String secret_key, String user_token, String user_key){
		client = new JumblrClient(consumer_key, secret_key, user_token, user_key);
	}*/
}


public class PostDeserializer implements JsonSerializer<Object>{

	@Override
	public Object deser(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException{
		JsonObject jobject = je.getAsJsonObject();
		String typeName = jobject.get("type").getAsString();
		String className = typeName.substring(0, 1).toUpperCase() + typeName.substring(1) + "Post";
		try {
			Class<?> clz = Class.forName("com.tumblr.jumblr.types." + className);
			return jdc.deserialize(je, clz);
		} catch (ClassNotFoundException e) {
			System.out.println("deserialized post for unknown type: " + typeName);
			return jdc.deserialize(je, UnknownTypePost.class);
		}
	}

	@Override
	public JsonElement serialize(Object src, Type typeOfSrc, JsonSerializationContext context){

		return null;
	}
}
