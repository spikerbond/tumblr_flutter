package alexislarson.tumblrflutter;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.util.HashMap;
//import java.util.LinkedHashMap;

public class IntentTumblr extends IntentService{
	public static final String PARAM_IN_MSG = "imsg";
	//public static final String PARAM_OUT_MSG = "omsg";
	private static final Tumblr tumblr = new Tumblr();
	public static final String ACTION_RESP = "com.alexislarson.intent.action.MESSAGE_PROCESSED";

	public IntentTumblr(){
		super("IntentTumblr");
	}

	@Override
	protected void onHandleIntent(Intent intent){
		HashMap<String, ?> msg = (HashMap) intent.getSerializableExtra(PARAM_IN_MSG);
		HashMap<String, ?> newMap = tumblr.getPosts(msg);

		//Log.wtf("intentTumblr", "get posts finished, "+newMap.size()+" posts");

		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(ACTION_RESP);
		//broadcastIntent.addCategory(Intent.category);
		broadcastIntent.putExtra("postMap", newMap);
		LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
		//Log.wtf("intentTumblr", "sent broadcast");
	}

}
