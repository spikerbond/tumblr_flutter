package alexislarson.tumblrflutter;

import android.os.AsyncTask;

import com.tumblr.jumblr.JumblrClient;
import com.tumblr.jumblr.types.Post;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import android.support.v4.widget.CircularProgressDrawable;


/**
 * Asynchronous task for getting posts of a specific blog
 */
class GetPosts extends AsyncTask<Void, Void, List<Post>>{
	private List<Post> posts = new ArrayList<>();
	private final Map<String, Object> params = new HashMap<>();
	private final JumblrClient client;

	GetPosts(Tumblr tumblr, HashMap<String, ?> args){
		params.put("reblog_info", true);
		params.put("notes_info", true);
		if(args.get("postType") != null)
			params.put("type", args.get("postType").toString());
		params.put("before_id", Long.parseLong(args.get("lastPostId").toString()));

		this.client = tumblr.client;
	}

	@Override
	protected List<Post> doInBackground(Void... asdf){
		posts = client.userDashboard(params);
		return posts;
	}
}
