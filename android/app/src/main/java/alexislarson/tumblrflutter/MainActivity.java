package alexislarson.tumblrflutter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.WindowManager.LayoutParams;

import java.util.HashMap;
import java.util.Objects;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.StandardMethodCodec;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.view.FlutterView;

public class MainActivity extends FlutterActivity{

	private static final String Jumblr_Channel = "jumblr";
	//private static final String Jumblr_event_Channel = "jumblr_event";
	//private static final Tumblr tumblr = new Tumblr();
	//protected String[] consumerKeys = {"Dc6U8xihAUKjkGj4EvZuwTQBLcp4GfghHdfTzox7Nr7hpH3CpU", "0TGlMByPXZF8lXaIgHqqLXWycGFqz0qPrGESifWQJeBRVMddSj"};
	//protected String[] userTokens = {"xJ3K3SDyQOwPnhAJ60k1Pn9yPUzYnyoF8pYRfaZaqHlwgvMDRk", "lXLeytgvzAmegImQOswWoEKbHHc6fdOvW7oOOLMvQn9b9hCPVJ"};

	private ResponseReceiver receiver;
	private IntentFilter filter;
	private Intent intentTumblr;
	static Context thisContext;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		thisContext = getContext();
		//tumblr.setClient(new JumblrClient(consumerKeys[0], consumerKeys[1], userTokens[0], userTokens[1]));
		GeneratedPluginRegistrant.registerWith(this);
		//println("created");

		//tumblr.prepareGetPosts();


		filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new ResponseReceiver();
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);

		//Log.wtf("get posts method call", "starting creation of intent");
		intentTumblr = new Intent(this, IntentTumblr.class);


		new MethodChannel(getFlutterView(), Jumblr_Channel).setMethodCallHandler(
				new MethodCallHandler(){
					@Override
					public void onMethodCall(MethodCall call, Result result){
						//println("Method called on java side, method: " + call.method + "; arguments: " + call.arguments);
						switch(call.method){
							case "getPosts":
								intentTumblr.putExtra(IntentTumblr.PARAM_IN_MSG, (HashMap) call.arguments);
								//Log.wtf("get posts method call", "starting service");
								startService(intentTumblr);
								//Log.wtf("get posts method call", "service started");
								result.success(true);
								//Log.wtf("method channel", "finished get posts");
								break;
							case "testing_createClient":
								//			tumblr.testing_createClient();
								result.success("Created client successfully.");
								break;
							default:
								result.error("Please provide a method", null, null);
								break;
						}
					}
				}
		);

	}

	@Override
	public void onDestroy(){
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
		super.onDestroy();
	}

	public static Context getContext(){
		return thisContext;
	}


	@Override
	public FlutterView createFlutterView(Context context){
		//println("create flutter view");
		final FlutterView view = new FlutterView(this);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		setContentView(view);
		//noinspection UnusedAssignment
		String route = "/";
		if(!getSharedPreferences("LoginInfo", MODE_PRIVATE).getAll().isEmpty()){
			route = "/HaveLogin";
		}else{
			route = getRouteFromIntent();
		}
		if(route != null){
			view.setInitialRoute(route);
			//println("route set");
		}
		//println("Set route: " + route);
		return view;
	}


	private String getRouteFromIntent(){
		final Intent intent = getIntent();
		if(Intent.ACTION_VIEW.equals(intent.getAction()) && Objects.requireNonNull(intent.getData()).getHost().equalsIgnoreCase("alexis-tumblr-app-te.firebaseapp.com")){
			return "/AfterLogin";
		}
		return null;
	}

	public class ResponseReceiver extends BroadcastReceiver{
		public static final String ACTION_RESP = "com.alexislarson.intent.action.MESSAGE_PROCESSED";

		@SuppressWarnings ("unchecked")
		@Override
		public void onReceive(Context context, Intent intent){
			if(intent.getAction() != null && intent.getAction().equals(ACTION_RESP)){

				HashMap<String, ?> posts = new HashMap<>();
				if(intent.getSerializableExtra("postMap") instanceof HashMap){
					posts = ((HashMap) intent.getSerializableExtra("postMap"));
				}
				//Log.wtf("ResponseReceiver", "recieved intent");
				//Log.wtf("ResponseReceiver", "posts size: " + posts.size());

				new MethodChannel(getFlutterView(), Jumblr_Channel, StandardMethodCodec.INSTANCE).invokeMethod("returnPosts", posts);
			}
		}

	}

}
