/*

 package alexislarson.tumblrflutter;
 import io.flutter.app.FlutterActivity;
 import io.flutter.view.FlutterView;
 import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
 import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
 import oauth.signpost.exception.OAuthCommunicationException;
 import oauth.signpost.exception.OAuthExpectationFailedException;
 import oauth.signpost.exception.OAuthMessageSignerException;
 import oauth.signpost.exception.OAuthNotAuthorizedException;

 import android.app.Activity;
 import android.content.Context;
 import android.content.Intent;
 import android.net.Uri;
 import android.os.Bundle;
 import android.util.Log;
 import android.view.WindowManager;

 public class TumblrLogin extends FlutterActivity{

 private static final String TAG = "TumblrLogin";

 private static final String REQUEST_TOKEN_URL = "https://www.tumblr.com/oauth/request_token";
 private static final String ACCESS_TOKEN_URL = "https://www.tumblr.com/oauth/access_token";
 private static final String AUTH_URL = "https://www.tumblr.com/oauth/authorize";

 // Taken from Tumblr app registration
 private static final String CONSUMER_KEY = "get consumer key from tumblr!!!";
 private static final String CONSUMER_SECRET = "get consumer secret from tumblr!!";

 private static final String CALLBACK_URL = "tumblrdemo://tumblrdemo.com/ok";


 @Override public void onCreate(Bundle savedInstanceState) {
 super.onCreate(savedInstanceState);
 //setContentView(R.layout.main);

 // To get the oauth token after the user has granted permissions
 Uri uri = this.getIntent().getData();
 if (uri != null) {

 String token = uri.getQueryParameter("oauth_token");
 String verifier = uri.getQueryParameter("oauth_verifier");

 Log.v(TAG, "Token:" +token);
 Log.v(TAG, "Verifier:" +verifier);
 } else {

 CommonsHttpOAuthConsumer consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY,
 CONSUMER_SECRET);

 // It uses this signature by default
 // consumer.setMessageSigner(new HmacSha1MessageSigner());

 CommonsHttpOAuthProvider provider = new CommonsHttpOAuthProvider(
 REQUEST_TOKEN_URL,
 ACCESS_TOKEN_URL,
 AUTH_URL);

 String authUrl;
 try {
 authUrl = provider.retrieveRequestToken(consumer, CALLBACK_URL);
 Log.v(TAG, "Auth url:" + authUrl);

 startActivity(new Intent("android.intent.action.VIEW", Uri.parse(authUrl)));

 } catch (OAuthMessageSignerException e) {
 // TODO Auto-generated catch block
 e.printStackTrace();
 } catch (OAuthNotAuthorizedException e) {
 // TODO Auto-generated catch block
 e.printStackTrace();
 } catch (OAuthExpectationFailedException e) {
 // TODO Auto-generated catch block
 e.printStackTrace();
 } catch (OAuthCommunicationException e) {
 // TODO Auto-generated catch block
 e.printStackTrace();
 }

 }

 }

 @Override public FlutterView createFlutterView(Context context) {
 final FlutterView view = new FlutterView(this);
 view.setLayoutParams(new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT));
 setContentView(view);
 final String route = getRouteFromIntent();
 if (route != null) {
 view.setInitialRoute(route);
 }
 return view;
 }

 private String getRouteFromIntent() {
 final Intent intent = getIntent();
 if (Intent.ACTION_VIEW.equals(intent.getAction()) && intent.getData() != null) {
 return intent.getData().getPath();
 }
 return null;
 }

 /*
  * (non-Javadoc)
  * @see android.app.Activity#onResume()

 @Override protected void onResume() {
 // TODO Auto-generated method stub
 super.onResume();

 Log.v(TAG, "onResume");
 }

 }
 */