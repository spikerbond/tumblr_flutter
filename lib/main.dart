import 'package:flutter/material.dart';
//import 'package:tumblr_flutter/screens/main.dart';

import 'package:tumblr_flutter/screens/MainScopedModel.dart';
main() {
  runApp(new AlexisTumblr());
}

class AlexisTumblr extends StatefulWidget {
  AlexisTumblrState createState() => new AlexisTumblrState();
}

class AlexisTumblrState extends State<AlexisTumblr> {

  @override
  Widget build(BuildContext context) {
    final routes = <String, WidgetBuilder>{
      // Shown when launched with plain intent.
      '/': (BuildContext ctx) => new MainPage(),
      // Shown when launched with known deep link.
      '/HaveLogin': (BuildContext ctx) => new MainPage(),
      // Shown when launched with another known deep link.
      '/AfterLogin': (BuildContext ctx) => new MainPage(),
    };

    return new MaterialApp(
      title: 'Alexis Tumblr',
      routes: routes,
      // Forces use of initial route from platform (otherwise it defaults to /
      // and platform's initial route is ignored).
      initialRoute: null,
      // Used when launched with unknown deep link.
      // May do programmatic parsing of routing path here.
      onGenerateRoute: (RouteSettings settings) => new MaterialPageRoute(
        builder: (BuildContext ctx) => centeredText('Not found'),
      ),
    );
  }

  Widget centeredText(String text) {
    return new Scaffold(body: new Center(child: new Text(text)));
  }
}
