import 'package:tumblr_flutter/tumblrAPI/PostConverter.dart';

class TestPosts {
  List<Post> testPosts = new List();

  List<Post> getTestPosts() {
    List<String> tags = new List<String>();
    for (int i = 0; i < 10; i++) tags.add("Tag " + i.toString());

    int noteCount = 100;
    List<Note> notes = new List<Note>();
    for (int n = 0; n < noteCount; n++) notes.add(new Note("Added text " + n.toString(), "Blog " + n.toString(), "www.blogurl.com", n, "Reply " + n.toString(), 1528217053 + (n * 100), "like"));

    testPosts.add(new TextPost(
        "Author",
        "Blog name",
        false,
        "Date posted",
        "Format",
        123456,
        false,
        1528217253,
        false,
        noteCount,
        notes,
        "www.posturl.com",
        654321,
        "Reblogged from name",
        "Rebloged from title",
        "www.reblogedfrom.com",
        00000001,
        "Root reblog name",
        "Root reblog title",
        "www.rootreblog.com",
        "reblogkey",
        "www.shorturl.com",
        "wtf is slug",
        "Source title",
        "www.sourceurl.com",
        "state",
        tags,
        1528217457,
        "text",
        '<h1>HTML Ipsum Presents</h1><p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p><h2>Header Level 2</h2><ol><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ol><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote><h3>Header Level 3</h3><ul><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ul><pre><code>#header h1 a {display: block;width: 300px;height: 80px;}</code></pre>',
        "Title"));

    testPosts.add(new AnswerPost("Author", "Blog name", false, "Date posted", "Format", 123456, false, 1528217253, false, noteCount, notes, "www.posturl.com", 654321, "Reblogged from name", "Rebloged from title", "www.reblogedfrom.com", 00000001, "Root reblog name", "Root reblog title", "www.rootreblog.com", "reblogkey", "www.shorturl.com", "wtf is slug", "Source title", "www.sourceurl.com", "state", tags, 1528217457, "answer", '<p>// No</p>', "Asking name", "www.askingUrl.com", 'JavaScript or Python?'));

    List<Photo> photos = new List();
    photos.add(new Photo("Square photo caption", 899, "http://www.getsomemagazine.com/wp-content/uploads/2018/04/Chvrches-Album-Cover.jpg", 899));
    photos.add(new Photo("wide photo caption", 395, "https://media.pitchfork.com/photos/5a96f1a8d63bda1b0a703087/2:1/w_790/14578-2Y3A5253.jpg", 790));
    photos.add(new Photo("Tall photo caption", 3336, "http://thelevel.my/wp-content/uploads/2016/09/IMG_9009.jpg", 2196));

    testPosts.add(new PhotoPost("Author", "Blog name", false, "Date posted", "Format", 123456, false, 1528217253, false, noteCount, notes, "www.posturl.com", 654321, "Reblogged from name", "Rebloged from title", "www.reblogedfrom.com", 00000001, "Root reblog name", "Root reblog title", "www.rootreblog.com", "reblogkey", "www.shorturl.com", "wtf is slug", "Source title", "www.sourceurl.com", "state", tags, 1528217457, "photo", "this is a photo caption", 899, photos, 899));

    testPosts.add(new AudioPost("Author", "Blog name", false, "Date posted", "Format", 123456, false, 1528217253, false, noteCount, notes, "www.posturl.com", 654321, "Reblogged from name", "Rebloged from title", "www.reblogedfrom.com", 00000001, "Root reblog name", "Root reblog title", "www.rootreblog.com", "reblogkey", "www.shorturl.com", "wtf is slug", "Source title", "www.sourceurl.com", "state", tags, 1528217457, "audio", "https://is5-ssl.mzstatic.com/image/thumb/Music/v4/2c/0c/f2/2c0cf2b4-f676-98fb-ac03-ba806ec8aca9/UMG_cvrart_00044003171008_01_RGB72_1500x1500_13CANIM00541.jpg/1200x630bb.jpg", "Album name", "Artist", "www.audioUrl.com", "this is a caption for an audio post", '<audio id="t-rex-roar" controls src="http://soundbible.com/mp3/Tyrannosaurus%20Rex%20Roar-SoundBible.com-807702404.mp3"> Your browser does not support the <code>audio</code> element. </audio>', 10, "Track name", 1, 2018));

    testPosts.add(new ChatPost(
        "Author",
        "Blog name",
        false,
        "Date posted",
        "Format",
        123456,
        false,
        1528217253,
        false,
        noteCount,
        notes,
        "www.posturl.com",
        654321,
        "Reblogged from name",
        "Rebloged from title",
        "www.reblogedfrom.com",
        00000001,
        "Root reblog name",
        "Root reblog title",
        "www.rootreblog.com",
        "reblogkey",
        "www.shorturl.com",
        "wtf is slug",
        "Source title",
        "www.sourceurl.com",
        "state",
        tags,
        1528217457,
        "chat",
        '<h1>HTML Ipsum Presents</h1><p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p><h2>Header Level 2</h2><ol><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ol><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote><h3>Header Level 3</h3><ul><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ul><pre><code>#header h1 a {display: block;width: 300px;height: 80px;}</code></pre>',
        null,
        "title"));

    testPosts.add(new LinkPost("Author", "Blog name", false, "Date posted", "Format", 123456, false, 1528217253, false, noteCount, notes, "www.posturl.com", 654321, "Reblogged from name", "Rebloged from title", "www.reblogedfrom.com", 00000001, "Root reblog name", "Root reblog title", "www.rootreblog.com", "reblogkey", "www.shorturl.com", "wtf is slug", "Source title", "www.sourceurl.com", "state", tags, 1528217457, "link", 'description', 'titile', 'www.google.com'));

    testPosts.add(new QuotePost("Author", "Blog name", false, "Date posted", "Format", 123456, false, 1528217253, false, noteCount, notes, "www.posturl.com", 654321, "Reblogged from name", "Rebloged from title", "www.reblogedfrom.com", 00000001, "Root reblog name", "Root reblog title", "www.rootreblog.com", "reblogkey", "www.shorturl.com", "wtf is slug", "Source title", "www.sourceurl.com", "state", tags, 1528217457, "quote", "source", 'text'));

    testPosts.add(new UnknownTypePost("Author", "Blog name", false, "Date posted", "Format", 123456, false, 1528217253, false, noteCount, notes, "www.posturl.com", 654321, "Reblogged from name", "Rebloged from title", "www.reblogedfrom.com", 00000001, "Root reblog name", "Root reblog title", "www.rootreblog.com", "reblogkey", "www.shorturl.com", "wtf is slug", "Source title", "www.sourceurl.com", "state", tags, 1528217457, "unknown"));

    testPosts.add(new VideoPost(
        "Author",
        "Blog name",
        false,
        "Date posted",
        "Format",
        123456,
        false,
        1528217253,
        false,
        noteCount,
        notes,
        "www.posturl.com",
        654321,
        "Reblogged from name",
        "Rebloged from title",
        "www.reblogedfrom.com",
        00000001,
        "Root reblog name",
        "Root reblog title",
        "www.rootreblog.com",
        "reblogkey",
        "www.shorturl.com",
        "wtf is slug",
        "Source title",
        "www.sourceurl.com",
        "state",
        tags,
        1528217457,
        "video",
        '<h2><b><i>LEEK</i> by Badun</b><br/>from the album <i>Last Night Sleep</i> (2016) on Mindwaves Music.</h2><h2><b>Listen to <i>MUSIC FOR PROGRAMMING</i> on SPOTIFY: </b><a href="http://t.umblr.com/redirect?z=https%3A%2F%2Fopen.spotify.com%2Fuser%2Fallaboutdrama%2Fplaylist%2F0a5W3jk18XsHCwgysV2nyP&amp;t=NjBmMTYyNmU2NjcxNmU0ZjY2ZDQyZGU4NGVkNWNmNjA0NGZhNjRmNixwdDJuY3dYZQ%3D%3D&amp;b=t%3ACbbLRji6mofJcwc4fYwb1w&amp;p=http%3A%2F%2Fmusicforprogramming.tumblr.com%2Fpost%2F169115701881%2Fa-midsummer-nice-dream-by-ochre-from-the-album-a&amp;m=1"></a><a href="https://t.umblr.com/redirect?z=http%3A%2F%2Fbit.ly%2Fmusic-4-programming&amp;t=ZDE5MjYwMmIzY2U0ZTNiZDBlMjZjNGIzZDFkNjk1Mzk0ZDBlZmY3NyxZM3Jkd04xUg%3D%3D&amp;b=t%3ACbbLRji6mofJcwc4fYwb1w&amp;p=http%3A%2F%2Fmusicforprogramming.tumblr.com%2Fpost%2F169851539021%2Fanenon-body&amp;m=1">http://bit.ly/music-4-programming</a></h2>',
        'https://www.youtube.com/watch?v=Yvt13R-VivY',
        360,
        'https://i.ytimg.com/vi/Yvt13R-VivY/hqdefault.jpg',
        480,
        null));

    return testPosts;
  }
}
