import 'dart:core';

import 'package:flutter/material.dart';

class PostConverter {
  List<Post> createList(List<Map> _postsData) {
    List<Post> temp = new List<Post>();
    //debugPrint("got to postCreator");
    //debugPrint("First post type is " + _postsData.elementAt(0)["type"]);
    //debugPrint("First post title is " + _postsData.elementAt(0)["title"].toString());
    for (int i = 0; i < _postsData.length; i++) {
      //debugPrint("creating post " + i.toString());
      temp.add(create(_postsData.elementAt(i)));
      //debugPrint("created post " + i.toString());
    }
    //debugPrint("postlist is now " + temp.length.toString() + " posts long!");
    return temp;
  }

  Post create(Map _newData) {
    ////debugPrint("got to create");
    List<Note> tempNotes = new List<Note>();
    ////debugPrint("created tempNotes");
    if (_newData["notes"] != null)
      for (int i = 0; i < _newData["notes"].length; i++) {
        ////debugPrint("creating note " + i.toString());
        Map value = _newData["notes"].elementAt(i);
        tempNotes.add(new Note(value["addedText"] as String, value["blogName"] as String, value["blogUrl"] as String, value["postId"] as int, value["replyText"] as String, value["timestamp"] as int, value["type"] as String));
      }
    //else
    ////debugPrint("no notes");
    List<String> tempTags = new List<String>();
    if (_newData["tags"] != null)
      for (var value in _newData["tags"]) {
        tempTags.add(value);
      }
    //else
    //debugPrint("no tags");

    switch (_newData["type"]) {
      case "chat":
        List<Dialogue> tempDialogue = new List<Dialogue>();
        for (var value in _newData["dialog"]) {
          tempDialogue.add(new Dialogue(value["label"] as String, value["name"] as String, value["phrase"] as String));
        }
        return new ChatPost(
            _newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["body"] as String, tempDialogue, _newData["title"] as String);

      case "video":
        List<Video> tempVideo = new List<Video>();
        for (var value in _newData["videos"]) {
          tempVideo.add(new Video(value["embedCode"] as String, value["width"] as int));
        }
        return new VideoPost(_newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["caption"] as String,
            _newData["permaLinkUrl"] as String, _newData["thumbnailHeight"] as int, _newData["thumbnailUrl"] as String, _newData["thumbnailWidth"] as int, tempVideo);

      case "photo":
        List<Photo> tempPhotos = new List<Photo>();
        for (var value in _newData["photos"]) {
          tempPhotos.add(new Photo(value["caption"] as String, value["originalHeight"] as int, value["originalUrl"] as String, value["originalWidth"] as int));
        }
        return new PhotoPost(_newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["caption"] as String,
            _newData["height"] as int, tempPhotos, _newData["width"] as int);

      case "answer":
        return new AnswerPost(_newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["answer"] as String,
            _newData["askingName"] as String, _newData["askingUrl"] as String, _newData["question"] as String);

      case "audio":
        return new AudioPost(_newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["albumArtUrl"] as String,
            _newData["albumName"] as String, _newData["artistName"] as String, _newData["audioUrl"] as String, _newData["caption"] as String, _newData["embedCode"] as String, _newData["playCount"] as int, _newData["trackName"] as String, _newData["trackNumber"] as int, _newData["year"] as int);

      case "link":
        return new LinkPost(_newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["description"] as String,
            _newData["linkUrl"] as String, _newData["title"] as String);

      case "quote":
        return new QuotePost(
            _newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["source"] as String, _newData["text"] as String);

      case "text":
        return new TextPost(_newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String, _newData["body"] as String, _newData["title"] as String);

      default:
        return new UnknownTypePost(_newData["author"] as String, _newData["blogName"] as String, _newData["bookmarklet"] as bool, _newData["date"] as String, _newData["format"] as String, _newData["id"] as int, _newData["liked"] as bool, _newData["likedTimestamp"] as int, _newData["mobile"] as bool, _newData["noteCount"] as int, tempNotes, _newData["postUrl"] as String, _newData["rebloggedFromId"] as int, _newData["rebloggedFromName"] as String, _newData["rebloggedFromTitle"] as String, _newData["rebloggedFromUrl"] as String, _newData["rebloggedRootId"] as int, _newData["rebloggedRootName"] as String, _newData["rebloggedRootTitle"] as String, _newData["rebloggedRootUrl"] as String, _newData["reblogKey"] as String, _newData["shortUrl"] as String, _newData["slug"] as String, _newData["sourceTitle"] as String, _newData["sourceUrl"] as String, _newData["state"] as String, tempTags, _newData["timestamp"] as int, _newData["type"] as String);
    }
  }
}

class ChatPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  ChatPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.body, this.dialogue, this.title);

  String title, body;
  List<Dialogue> dialogue = new List<Dialogue>();
}

class Dialogue {
  String name, label, phrase;

  Dialogue(this.label, this.name, this.phrase);
}

class VideoPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  VideoPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.caption, this.permalinkUrl, this.thumbnailHeight, this.thumbnailUrl, this.thumbnailWidth, this.videos);

  List<Video> videos = new List<Video>();
  String caption, permalinkUrl, thumbnailUrl;
  int thumbnailWidth, thumbnailHeight;

//File = _data;
}

class Video {
  int width;
  String embedCode;

  Video(this.embedCode, this.width);
}

class PhotoPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  PhotoPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.caption, this.height, this.photos, this.width);

  String caption;
  int width, height;
  List<Photo> photos = new List<Photo>();
//  List<Photo> pendingPhotos;
}

class Photo {
  String caption, url;
  int width, height;

  //  File file;
  Photo(this.caption, this.height, this.url, this.width);
}

class Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

//  Post(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type);

}

class Note {
  int timestamp;
  String blogName;
  String blogUrl;
  String type;
  int postId;
  String replyText;
  String addedText;

  Note(this.addedText, this.blogName, this.blogUrl, this.postId, this.replyText, this.timestamp, this.type);
}

class TextPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  TextPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.body, this.title);

  String title, body;
}

class AnswerPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  AnswerPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.answer, this.askingName, this.askingUrl, this.question);

  String askingName, askingUrl, question, answer;
}

class AudioPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  AudioPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.albumArtUrl, this.albumName, this.artist, this.audioUrl, this.caption, this.embedCode, this.playCount, this.trackName, this.trackNumber, this.year);

  String caption, audioUrl, albumArtUrl, albumName, artist, trackName, embedCode;
  int trackNumber, year, playCount;
}

class LinkPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  LinkPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.description, this.title, this.url);

  String title, url, description;
}

class QuotePost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  QuotePost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.source, this.text);

  String text, source;
}

class UnknownTypePost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  UnknownTypePost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type);
}

/*
class PostcardPost extends Post {
  String type, author, reblogKey, blogName, postUrl, shortUrl, state, format, date, sourceUrl, sourceTitle, slug, rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle, rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  int noteCount, id, timestamp, likedTimestamp, rebloggedFromId, rebloggedRootId;
  bool bookmarklet, mobile, liked;
  List<String> tags = new List<String>();
  List<Note> notes = new List<Note>();

  PostcardPost(this.author, this.blogName, this.bookmarklet, this.date, this.format, this.id, this.liked, this.likedTimestamp, this.mobile, this.noteCount, this.notes, this.postUrl, this.rebloggedFromId, this.rebloggedFromName, this.rebloggedFromTitle, this.rebloggedFromUrl, this.rebloggedRootId, this.rebloggedRootName, this.rebloggedRootTitle, this.rebloggedRootUrl, this.reblogKey, this.shortUrl, this.slug, this.sourceTitle, this.sourceUrl, this.state, this.tags, this.timestamp, this.type, this.askingName, this.askingUrl, this.body);

  String body, askingName, askingUrl;
}
*/
/*
class User {
  List<Blog> blogs = new List<Blog>();
  String name;

  //dynamic following;
  int likes;
  String defaultPostFormat;

  User(Map _data) {}
}

class Blog {
  String name;
  String title;
  String description;
  int posts, likes, followers;
  int updated;
  bool ask, askAnon;

  Blog(Map _data) {}
}*/
