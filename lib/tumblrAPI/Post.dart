class Post {
  String type;
  double id;
  String author;
  String reblogKey;
  String blogName;
  String postUrl, shortUrl;
  double timestamp;
  double likedTimestamp;
  String state;
  String format;
  String date;
  bool bookmarklet, mobile;
  String sourceUrl;
  String sourceTitle;
  bool liked;
  String slug;
  double rebloggedFromId, rebloggedRootId;
  String rebloggedFromUrl, rebloggedFromName, rebloggedFromTitle;
  String rebloggedRootUrl, rebloggedRootName, rebloggedRootTitle;
  double noteCount;
  List<String> tags;
  List<Note> notes;
}

class AnswerPost extends Post {
  String askingName, askingUrl;
  String question;
  String answer;
}

class AudioPost extends Post {
  String caption, player, audio_url;
  int plays;
  String album_art, artist, album, track_name;
  int track_number, year;
  String external_url;
//File data;
}

class Blog {
  String name;
  String title;
  String description;
  int posts, likes, followers;
  double updated;
  bool ask, ask_anon;
}

class ChatPost extends Post {
  String title;
  String body;
  List<Dialogue> dialogue;
}

class Dialogue {
  String name;
  String label;
  String phrase;
}

class LinkPost extends Post {
  String title;
  String url;
  String description;
}

class Note {
  double timestamp;
  String blog_name;
  String blog_url;
  String type;
  double post_id;
  String reply_text;
  String added_text;
}

class Photo {
  String caption;
  List<PhotoSize> alt_sizes;
  PhotoSize original_size;
  String source;
//  File file;
}

class PhotoPost extends Post {
  String caption;
  int width, height;
  String link;

  // TODO: Do not leak the photos member variable to world
  List<Photo> photos;
  List<Photo> pendingPhotos;
}

class PhotoSize {
  int width, height;
  String url;
}

class PostcardPost extends Post {
  String body;
  String askingName;
  String askingUrl;
}

class QuotePost extends Post {
  String text;
  String source;
}

class TextPost extends Post {
  String title;
  String body;
}

class UnknownTypePost extends Post {}

class User {
  List<Blog> blogs;
  String name;

  //dynamic following;
  int likes;
  String default_post_format;
}

class Video {
  int width;
  String embed_code;
}

class VideoPost extends Post {
  List<Video> player;
  String caption;
  String embed, permalink_url;
  String thumbnail_url;
  int thumbnail_width;
  int thumbnail_height;
//File data;
}
