import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tumblr_flutter/screens/postCards.dart';
import 'package:tumblr_flutter/tumblrAPI/PostConverter.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //MainBody mainBody = new MainBody();
    return new MaterialApp(theme: new ThemeData.light(), title: "Mumblr", home: new MainBody());
  }
}

class MainBody extends StatefulWidget {
  MainBodyState createState() => new MainBodyState();
}

class MainBodyState extends State<MainBody> with SingleTickerProviderStateMixin {

  final GlobalKey<ScaffoldState> mainScaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<ScrollableState> _customScrollKey = new GlobalKey<ScrollableState>();
  final GlobalKey<RefreshIndicatorState> _refreshKey = new GlobalKey<RefreshIndicatorState>();

  MethodChannel javaChannel;
  AnimationController _controller;
  Drawer drawer;
  Widget currentScreen;
  Widget body;
  Widget futureBuilder;
  Widget sliverList;
  List<Widget> cards = new List<Widget>();
  SliverAppBar appBar;
  List<Post> posts = new List<Post>();
  ScrollPositionWithSingleContext scrollPos;
  ScrollController scrollController = new ScrollController();
  bool canGetMorePosts = true;
  bool finishSetup = false;
  bool waitingForPosts = false;
  String postType;
  SliverChildBuilderDelegate cardBuilder;
  List<String> dropdownList = ["All", "Pictures", "Text", "Quote", "Link", "Chat", "Audio", "Video", "Answer"];
  String dropdownselected;

  @override
  void initState() {
    super.initState();
    javaChannel = const MethodChannel('jumblr');

    javaChannel.setMethodCallHandler((call) {
      switch (call.method) {
        case "returnPosts":
          {
            //debugPrint("got returnPosts call");
            //debugPrint("arguments type "+call.arguments.runtimeType.toString());
            var args = call.arguments;
            //debugPrint("casted arguments to var of type "+args.runtimeType.toString());
            List<Map> argsList = (args["posts"] as List).cast<Map>().toList();
            //Map<String, dynamic> argsMap = new Map<String, dynamic>.from(args);
            List<Post> postsList = new PostConverter().createList(argsList);

            returnPosts(postsList);
            break;
          }
        case "timeout":
          {

          }
      }
    });
    _controller = new AnimationController(debugLabel: "get more posts indicator", duration: new Duration(seconds: 30), vsync: this,);
    _controller.addListener(() {
      this.setState(() {
      });
    });

    scrollController.addListener(() {
      scrollPosChange();
    });
  }

  @override
  void dispose() {
    super.dispose();
    //_controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(body: createNonFutureBody(), drawer: createDrawer(context), floatingActionButton: new FloatingActionButton(onPressed: () {
      _getPosts();
    }, child: const Icon(Icons.refresh),),);
  }

  setScreen(Widget screen) {
    currentScreen = screen;
  }

  Widget createNonFutureBody() {
    return new RefreshIndicator(key: _refreshKey, child: new Stack(children: <Widget>[new Positioned(child: new CustomScrollView(physics: new AlwaysScrollableScrollPhysics(), key: _customScrollKey, slivers: <Widget>[new SliverAppBar(title: new Text("Mumblr"), floating: true, actions: appBarActions(),), new SliverList(delegate: new SliverChildBuilderDelegate((context, index) {
      //debugPrint("postmap length"+posts.length.toString());
      // debugPrint("building card "+index.toString());
      if (posts.length == 0) {
        //getMorePosts();
      } else if (index == posts.length) {
        //waitingForPosts = false;
        //_controller.reverse(from: 0.0);
        // _getPostsCompleter.complete();
        //return ;
      } else {
        //debugPrint("creating post " + index.toString());
        return new PostCards(posts.elementAt(index));
      }
    }, childCount: posts.length),),
    ], controller: scrollController), top: 0.0, left: 0.0, right: 0.0, bottom: 0.0,), new Positioned(child: _controller.isAnimating ? new Padding(child: new LinearProgressIndicator(), padding: new EdgeInsets.fromLTRB(0.0, 3.0, 0.0, 0.0),) : new Container(), bottom: 0.0, left: 0.0, right: 0.0,),
    ],), onRefresh: _getPosts,);
  }

  List<Widget> appBarActions() {
    return <Widget>[new IconButton(icon: const Icon(Icons.refresh), tooltip: 'Refresh dashboard', onPressed: () {
      _getPosts();
    },), new PopupMenuButton(onSelected: (result) {
      //setState(() {});
    }, itemBuilder: (context) =>
    <PopupMenuEntry>[const PopupMenuItem(child: const Text('Menu item 1'),), const PopupMenuItem(child: const Text('Menu item 2'),), const PopupMenuItem(child: const Text('Menu item 3'),), const PopupMenuItem(child: const Text('Menu item 4'),), const PopupMenuItem(child: const Text('Menu item 5'),), const PopupMenuItem(child: const Text('Menu item 6'),),
    ],)
    ];
  }

  scrollPosChange() {
    //debugPrint("want to get more posts");
    if (!waitingForPosts && scrollController.position.pixels == scrollController.position.maxScrollExtent) {
      debugPrint("actually trying to get more posts");
      _getMorePosts();
    }
  }

  Completer<Null> _getPostsCompleter;

  Future<Null> _getPosts() {
    if (!waitingForPosts) {
      _refreshKey.currentState.show();
      _getPostsCompleter = new Completer<Null>();
      //_controller.forward();
      posts.clear();
      waitingForPosts = true;
      // setState(() => waitingForPosts = true);
      Map<String, dynamic> args = new Map<String, dynamic>();
      args["postType"] = postType;
      args["lastPostId"] = 0;
      javaChannel.invokeMethod('getPosts', args);
    }
    return _getPostsCompleter.future;
  }

  _getMorePosts() {
    if (!waitingForPosts) {
      _controller.forward();
      waitingForPosts = true;
      setState(() => waitingForPosts = true);
      Map<String, dynamic> args = new Map<String, dynamic>();
      args["postType"] = postType;

      ///debugPrint("does this look like a correct ID? " + posts.elementAt(posts.length - 1).id.toString());
      args["lastPostId"] = posts
          .elementAt(posts.length - 1)
          .id;
      javaChannel.invokeMethod('getPosts', args);
    }
  }

  returnPosts(List<Post> newList) {
    //debugPrint("got to returnPosts");

    posts.addAll(newList.where((_) => true));
    //debugPrint("new posts size and type " + posts.length.toString() + " " + posts.runtimeType.toString());
    setState(() {
      waitingForPosts = false;
      _controller.reverse(from: 0.0);
      _getPostsCompleter.complete();
    });
  }

  Drawer createDrawer(BuildContext context) {
    return new Drawer(child: new ListView(padding: EdgeInsets.zero, children: <Widget>[new DrawerHeader(child: new Text(''), decoration: new BoxDecoration(color: Colors.blue,),), new ListTile(leading: const Icon(Icons.dashboard), title: const Text('Dashboard'), onTap: () {
      //setState(() {// mainView = dashboardScreen;},);
      Navigator.pop(context);
    },), new ListTile(leading: const Icon(Icons.favorite), title: const Text('Likes'), onTap: () {
      //setState(() {// mainView = dashboardScreen;},);
      Navigator.pop(context);
    },), new ListTile(leading: const Icon(Icons.people), title: const Text('Following'), onTap: () {
      //setState(() {// mainView = dashboardScreen;},);
      Navigator.pop(context);
    },), new ListTile(leading: const Icon(Icons.account_circle), title: const Text('Username_here'), onTap: () {
      //setState(() {// mainView = dashboardScreen;},);
      Navigator.pop(context);
    },), new Divider(height: 2.0,), new ListTile(leading: const Icon(Icons.settings), title: const Text('Settings'), onTap: () {
      //setState(() {// mainView = dashboardScreen;},);
      Navigator.pop(context);
    },),
    ],),);
  }
}
