import 'dart:core';

import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tumblr_flutter/screens/postCards.dart';
import 'package:tumblr_flutter/tumblrAPI/PostConverter.dart';
import 'package:tumblr_flutter/test_posts.dart';

final GlobalKey<ScrollableState> _customScrollKey = new GlobalKey<ScrollableState>();
//final GlobalKey<RefreshIndicatorState> _refreshKey = new GlobalKey<RefreshIndicatorState>();
final MethodChannel javaChannel = const MethodChannel('jumblr');
final ScrollController scrollController = new ScrollController();
final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

main() {
  runApp(MainPage());
}

class MainModel extends Model {
  List<Post> posts = new List<Post>();
  bool waitingForPosts = false;
  AnimationController _controller;
  AnimationController get controller => _controller;

  TextPost firsttext;
  PhotoPost firstphoto;
  VideoPost firstvideo;
  AnswerPost firstanswer;
  AudioPost firstaudio;
  ChatPost firstchat;
  LinkPost firstlink;
  QuotePost firstquote;
  UnknownTypePost firstunknown;

  MainModel() {
    javaChannel.setMethodCallHandler((call) {
      switch (call.method) {
        case "returnPosts":
          {
            debugPrint("got returnPosts call");
            //debugPrint("arguments type "+call.arguments.runtimeType.toString());
            var args = call.arguments;
            //debugPrint("casted arguments to var of type "+args.runtimeType.toString());
            List<Map> argsList = (args["posts"] as List).cast<Map>().toList();
            //Map<String, dynamic> argsMap = new Map<String, dynamic>.from(args);
            List<Post> postsList = new PostConverter().createList(argsList);

            returnPosts(postsList);
            break;
          }
        case "timeout":
          {}
      }
    });

    scrollController.addListener(() {
      scrollPosChange();
    });
  }

  void getPosts() {
    if (!waitingForPosts) {
      waitingForPosts = true;
      //_refreshKey.currentState.show();
      posts.clear();
      Map<String, dynamic> args = new Map<String, dynamic>();
      args["lastPostId"] = 0;
      javaChannel.invokeMethod('getPosts', args);
      //waitingForPosts = false;
      //_controller.reverse(from: 0.0);

      //posts.addAll(new TestPosts().getTestPosts());
      notifyListeners();
    }
  }

  void getMorePosts() {
	  if (false) {
		  posts.addAll(new TestPosts().getTestPosts());

		  //_controller.forward();
      waitingForPosts = true;
      Map<String, dynamic> args = new Map<String, dynamic>();
      args["lastPostId"] = posts.elementAt(posts.length - 1).id;
		  //args["type"] = "answer";
		  //javaChannel.invokeMethod('getPosts', args);

      notifyListeners();
    }
  }

  void returnPosts(List<Post> newList) {
    debugPrint("got to returnPosts");
    posts.addAll(newList);
    waitingForPosts = false;
    //_controller.reverse(from: 0.0);
    notifyListeners();
    debugPrint("notified");

	  /*
    for (var post in newList) {
      switch (post.type) {
        case "photo":
          {
            if (firstphoto == null) {
              firstphoto = post;
              debugPrint("Photopost width: " + firstphoto.width.toString());
              debugPrint("Photopost height: " + firstphoto.height.toString());
              debugPrint("Photopost width: " + firstphoto.width.toString());
              debugPrint("Photopost width: " + firstphoto.width.toString());
              int i = 0;
              firstphoto.photos.forEach((photo) {
                debugPrint("Photopost photo " + i.toString() + " caption: " + photo.caption);
                debugPrint("Photopost photo " + i.toString() + " url: " + photo.url);
                debugPrint("Photopost photo " + i.toString() + " width: " + photo.width.toString());
                debugPrint("Photopost photo " + i.toString() + " height: " + photo.height.toString());
                i++;
              });
            }
            break;
          }
        case "text":
          {
            if (firsttext == null) firsttext = post;
            break;
          }
        case "answer":
          {
            if (firstanswer == null) {
              firstanswer = post;
              debugPrint("answerpost question: " + firstanswer.question);
              debugPrint("answerpost answer: " + firstanswer.answer);
            }
            break;
          }
        case "audio":
          {
            if (firstaudio == null) firstaudio = post;
            break;
          }
        case "chat":
          {
            if (firstchat == null) {
              firstchat = post;
              debugPrint("chatpost title: " + firstchat.title);
              debugPrint("chatpost body: " + firstchat.body);
              int i = 0;
              firstchat.dialogue.forEach((value) {
                debugPrint("chatpost dialogue " + i.toString() + " name: " + value.name);
                debugPrint("chatpost dialogue " + i.toString() + " label: " + value.label);
                debugPrint("chatpost dialogue " + i.toString() + " phrase: " + value.phrase);
                i++;
              });
            }
            break;
          }
        case "link":
          {
            if (firstlink == null) firstlink = post;
            break;
          }
        case "quote":
          {
            if (firstquote == null) {
              firstquote = post;
              debugPrint("quotepost source: " + firstquote.source);
            }
            break;
          }
        case "video":
          {
            if (firstvideo == null) {
              firstvideo = post;
              debugPrint("videopost caption: " + firstvideo.caption);
              debugPrint("videopost permalink: " + firstvideo.permalinkUrl);
              debugPrint("videopost thumbnail: " + firstvideo.thumbnailUrl);
              debugPrint("videopost thumbnailheight: " + firstvideo.thumbnailHeight.toString());
              debugPrint("videopost thumbnailwidth: " + firstvideo.thumbnailWidth.toString());
              int i = 0;
              firstvideo.videos.forEach((video){
	              debugPrint("videopost video "+i.toString()+" width: " + video.width.toString());
	              debugPrint("videopost video "+i.toString()+" code: " + video.embedCode);

              	i++;
              });
            }
            break;
          }
        default:
          {
            if (firstunknown == null) firstunknown = post;
            break;
          }
      }
    }*/
  }

  scrollPosChange() {
	  debugPrint("want to get more posts");
    if (!waitingForPosts && scrollController.position.pixels == scrollController.position.maxScrollExtent) {
	    debugPrint("actually trying to get more posts");
      getMorePosts();
    }
  }
}

class MainPage extends StatelessWidget {
  final MainModel mainModel = new MainModel();

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData.light(),
      title: "Mumblr",
      home: new ScopedModel<MainModel>(
        model: mainModel,
        child: new Scaffold(
          key: _scaffoldKey,
          body: createBody(context),
          drawer: createDrawer(context),
          floatingActionButton: new FloatingActionButton(
            onPressed: () {
	            mainModel.getPosts();
            },
            child: const Icon(Icons.refresh),
          ),
        ),
      ),
    );
  }

  Widget createBody(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new Positioned(
          child: new CustomScrollView(physics: new AlwaysScrollableScrollPhysics(),
              key: _customScrollKey,
              slivers: <Widget>[
                new SliverAppBar(title: new Text("Mumblr"),
                  floating: true,
                  actions: appBarActions(),
                                 ),
                new ScopedModelDescendant<MainModel>(
                  rebuildOnChange: true,
                  builder: (context, child, model) => new SliverList(
                        delegate: new SliverChildBuilderDelegate((context, index) => new PostCards(model.posts.elementAt(index)), childCount: model.posts.length),
                      ),
                )
                /*
                new SliverFillRemaining(
                  child: new ScopedModelDescendant<MainModel>(
                    rebuildOnChange: true,
                    builder: (context, child, model) => new ListView.builder(
                          itemBuilder: (context, index) => new PostCards(model.posts.elementAt(index)),
                          itemExtent: 500.0,
                          itemCount: model.posts.length,
                        ),
                  ),
                ),*/
              ],
              controller: scrollController),
          top: 0.0,
          left: 0.0,
          right: 0.0,
          bottom: 0.0,
        ),
        new ScopedModelDescendant<MainModel>(
          builder: (context, child, model) => new Positioned(
                child: model.waitingForPosts
                    ? new Padding(
                        child: new LinearProgressIndicator(),
                        padding: EdgeInsets.fromLTRB(0.0, 3.0, 0.0, 0.0),
                      )
                    : new Container(),
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
              ),
        )
      ],
    );
  }

  List<Widget> appBarActions() {
    return <Widget>[
      new IconButton(
        icon: const Icon(Icons.refresh),
        tooltip: 'Refresh dashboard',
        onPressed: () {
          mainModel.getPosts();
        },
      ),
      new PopupMenuButton(
        onSelected: (result) {
          //setState(() {});
        },
        itemBuilder: (context) => <PopupMenuEntry>[
              const PopupMenuItem(
                child: const Text('Menu item 1'),
              ),
              const PopupMenuItem(
                child: const Text('Menu item 2'),
              ),
              const PopupMenuItem(
                child: const Text('Menu item 3'),
              ),
              const PopupMenuItem(
                child: const Text('Menu item 4'),
              ),
              const PopupMenuItem(
                child: const Text('Menu item 5'),
              ),
              const PopupMenuItem(
                child: const Text('Menu item 6'),
              ),
            ],
      )
    ];
  }

  Drawer createDrawer(BuildContext context) {
    return new Drawer(
      child: new ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          new DrawerHeader(
            child: new Text(''),
            decoration: new BoxDecoration(
              color: Colors.blue,
            ),
          ),
          new ListTile(
            leading: const Icon(Icons.dashboard),
            title: const Text('Dashboard'),
            onTap: () {
              //setState(() {// mainView = dashboardScreen;},);
              Navigator.pop(context);
            },
          ),
          new ListTile(
            leading: const Icon(Icons.favorite),
            title: const Text('Likes'),
            onTap: () {
              //setState(() {// mainView = dashboardScreen;},);
              Navigator.pop(context);
            },
          ),
          new ListTile(
            leading: const Icon(Icons.people),
            title: const Text('Following'),
            onTap: () {
              //setState(() {// mainView = dashboardScreen;},);
              Navigator.pop(context);
            },
          ),
          new ListTile(
            leading: const Icon(Icons.account_circle),
            title: const Text('Username_here'),
            onTap: () {
              //setState(() {// mainView = dashboardScreen;},);
              Navigator.pop(context);
            },
          ),
          new Divider(
            height: 2.0,
          ),
          new ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: () {
              //setState(() {// mainView = dashboardScreen;},);
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
/*
class LoadingAnimation extends AnimatedWidget {
  final MainModel model;

  LoadingAnimation(MainModel newModel) : model = newModel;

  Widget build(BuildContext context) {
    return new Positioned(
      child: model.waitingForPosts
          ? new Padding(
              child: new LinearProgressIndicator(),
              padding: new EdgeInsets.fromLTRB(0.0, 3.0, 0.0, 0.0),
            )
          : new Container(),
      bottom: 0.0,
      left: 0.0,
      right: 0.0,
    );
  }
}*/
