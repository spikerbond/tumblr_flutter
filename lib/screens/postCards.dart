import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:scoped_model/scoped_model.dart';
import 'package:tumblr_flutter/tumblrAPI/PostConverter.dart';

class CardModel extends Model {
	Post postData;

	CardModel(Post postData) {
		this.postData = postData;
		if (postData.liked == null) postData.liked = false;
	}

	FlatButton likeBtn() {
		debugPrint("like btn");
		return new FlatButton.icon(icon: postData.liked ? new Icon(Icons.favorite) : new Icon(Icons.favorite_border), onPressed: () {
			if (postData.liked)
				postData.liked = false;
			else
				postData.liked = true;
			notifyListeners();
		}, label: new Container(),);
	}

	FlatButton reblogBtn() {
		debugPrint("reblog btn");
		return new FlatButton.icon(icon: new Icon(Icons.repeat), onPressed: () {
			notifyListeners();
		}, label: new Container(),);
	}

	FlatButton shareBtn() {
		debugPrint("share btn");
		return new FlatButton.icon(icon: new Icon(Icons.share), onPressed: () {
			notifyListeners();
		}, label: new Container(),);
	}

	FlatButton noteBtn() {
		debugPrint("note btn");
		return new FlatButton.icon(icon: new Icon(Icons.speaker_notes), onPressed: () {
			notifyListeners();
		}, label: new Container(),);
	}
}

class PostCards extends StatelessWidget {
	final Post postData;
	final CardModel cardModel;

	//final BuildContext mainContext;
	final UniqueKey _cardKey = new UniqueKey();

	PostCards(Post postData)
			: this.postData = postData,
				cardModel = new CardModel(postData);

  @override
  Widget build(BuildContext context) {
	  return new ScopedModel<CardModel>(model: cardModel, child: new Card(key: _cardKey, color: Theme
			  .of(context)
			  .cardColor, margin: new EdgeInsets.all(3.0), child: new Column(mainAxisSize: MainAxisSize.min, children: <Widget>[new ListTile(leading: new CircleAvatar(child: new Text("Bn"),), title: new Text("Blogname: " + postData.blogName.toString()), subtitle: new Text("Post timestamp: " + postData.timestamp.toString() + "\nPost type: " + postData.type.toString(), softWrap: true,), isThreeLine: true, dense: true,), cardBody(context), // new Divider(),
	  //new Text("Type: " + data["post 1"]["type"]),
	  new ButtonTheme.bar(// make buttons use the appropriate styles for cards
	                      child: new ScopedModelDescendant<CardModel>(rebuildOnChange: true, builder: (context, child, model) =>
	                      new ButtonBar(children: <Widget>[new Text(postData.noteCount.toString() + " Notes"), model.noteBtn(), model.shareBtn(), model.reblogBtn(), model.likeBtn(),
	                      ],),),),
	  ],),),);
  }

	Widget cardBody(BuildContext context) {
		switch (postData.type) {
			case "photo":
				return photo(postData, context);

			case "text":
				return text(postData, context);

			case "answer":
				{
					return answer(postData, context);
				}
		/*
		  case "audio":{
		  	return audio();

		  }
		  case "chat":{
return chat();
		  }
		  case "link":{
		  	return link();
		  }
		  case "quote":{
		  	return quote();
		  }
		  case "video":{
		  	return video();
		  }*/default:
			{
				//Unknown type
				//return text();
				return new Container();
			}
		}
  }

	//double _markdownBodyHeight = 100.0;
	//UniqueKey _textMarkdownBody = new UniqueKey();
	//bool _textExpanded = false;
	//RenderBox box;

	Widget text(TextPost post, BuildContext context) {
    return new Padding(padding: new EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0), child: new Column(children: <Widget>[new ListTile(title: post.title != null ? new Text(post.title,
                                                                                                                                                                      //style: ThemeData().textTheme.title,
                                                                                                                                                                      ) : new Container(),), new ListView(children: <Widget>[new MarkdownBody(data: html2md.convert(post.body), styleSheet: new MarkdownStyleSheet.fromTheme(Theme.of(context)),),
    ], shrinkWrap: true, physics: new NeverScrollableScrollPhysics(),)
    ], mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.spaceAround,
      ),
                       );
	}

	Widget photo(PhotoPost post, BuildContext context) {
		double maxHeight = 100.0;
		post.photos.forEach((photo) {
			if (photo.height.toDouble() > maxHeight) maxHeight = photo.height.toDouble();
		});

		return new Padding(padding: new EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0), child: new Container() /*new PageView.builder(
        pageSnapping: true, //physics: new PageScrollPhysics(),
        itemBuilder: (context, index) {
          return new Image.network(
            post.photos.elementAt(index).url, repeat: ImageRepeat.noRepeat, fit: BoxFit.contain,
            //scale: 1.0,
          );
        },
        scrollDirection: Axis.horizontal, //shrinkWrap: true,
        itemCount: post.photos.length,

        //expandingHTML(data["photos"]["photo " + index.toString()]["caption"].toString())
      ),*/);
		/*
		postMap.put("caption", temp.getCaption());
		postMap.put("height", temp.getHeight());
		postMap.put("width", temp.getWidth());

		Iterator<Photo> photoIterator = temp.getPhotos().iterator();
		i = 0;
		HashMap<String, Object> temp_photo_map = new HashMap<>();
		while (photoIterator.hasNext()) {
			Photo tempPhoto = photoIterator.next();
			HashMap<String, Object> temp_map = new HashMap<>();
			temp_map.put("caption", tempPhoto.getCaption());
			temp_map.put("originalHeight", tempPhoto.getOriginalSize().getHeight());
			temp_map.put("originalUrl", tempPhoto.getOriginalSize().getUrl());
			temp_map.put("originalWidth", tempPhoto.getOriginalSize().getWidth());

			temp_photo_map.put("photo " + i, temp_map);
			i++;
		}
		postMap.put("photos", temp_photo_map);*/
  }

	Widget answer(AnswerPost post, BuildContext context) {
		debugPrint("question " + post.question);
		debugPrint("answer " + post.answer);
		return new Column(children: <Widget>[new ListTile(title: new Text(post.askingName + ':', textAlign: TextAlign.left,
		                                                                  //textScaleFactor: 1.25,
		                                                                  ), subtitle: new Text(post.question + "im gonna make this a super long question la al al;lkjasd;lkfjl  jlksda fjl;kasdjfl;kjasdl;k fjlksadj flksdj flksj fl;k salkdjf lkasdfj lksadjf lksajdf;lk ajsdflk;adjs flkja sdl ;kfjasl;d fjlkdsajf lksdjf lk;ajf ls;dkajf lksd;ajf l;sadjfl; ajsdsdal;kf jla;sdfj ;lasdfkj lasdkjf ;lkasdjfl;k jasdlfj kl;asdjfkl ;asjdfkl; ajsdfkl;j asl;dlfkj ;alskdfj ;lkasdjf l;asd;l kfj asdfl;kj", textAlign: TextAlign.left,),), new Padding(padding: new EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0), child: new Column(children: <Widget>[new Divider(), new MarkdownBody(data: html2md.convert(post.answer), styleSheet: new MarkdownStyleSheet.fromTheme(Theme.of(context)),),
		],),),
		], mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.spaceAround,);
	}

/*
  Widget audio(){
	  AudioPost temp = (AudioPost) post;
	  postMap.put("albumArtUrl", temp.getAlbumArtUrl());
	  postMap.put("albumName", temp.getAlbumName());
	  postMap.put("artistName", temp.getArtistName());
	  postMap.put("audioUrl", temp.getAudioUrl());
	  postMap.put("caption", temp.getCaption());
	  postMap.put("embedCode", temp.getEmbedCode());
	  postMap.put("playCount", temp.getPlayCount());
	  postMap.put("trackName", temp.getTrackName());
	  postMap.put("trackNumber", temp.getTrackNumber());
	  postMap.put("year", temp.getYear());

  }
  Widget chat(){
	  ChatPost temp = (ChatPost) post;
	  postMap.put("body", temp.getBody());
	  postMap.put("dialog", temp.getDialogue());
	  postMap.put("title", temp.getTitle());

  }
  Widget link(){
	  LinkPost temp = (LinkPost) post;
	  postMap.put("description", temp.getDescription());
	  postMap.put("linkUrl", temp.getLinkUrl());
	  postMap.put("title", temp.getTitle());

  }

  Widget quote(){
	  QuotePost temp = (QuotePost) post;
	  postMap.put("source", temp.getSource());
	  postMap.put("text", temp.getText());

  }

  Widget video(){
	  VideoPost temp = (VideoPost) post;
	  postMap.put("caption", temp.getCaption());
	  postMap.put("permaLinkUrl", temp.getPermalinkUrl());
	  postMap.put("thumbnailHeight", temp.getThumbnailHeight());
	  postMap.put("thumbnailUrl", temp.getThumbnailUrl());
	  postMap.put("thumbnailWidth", temp.getThumbnailWidth());

	  Iterator<Video> videoIterator = temp.getVideos().iterator();
	  i = 0;
	  HashMap<String, Object> temp_video_map = new HashMap<>();
	  while(videoIterator.hasNext()){
		  Video tempVideo = videoIterator.next();
		  HashMap<String, Object> temp_map = new HashMap<>();
		  temp_map.put("embedCode", tempVideo.getEmbedCode());
		  temp_map.put("width", tempVideo.getWidth());

		  temp_video_map.put("video " + i, temp_map);
		  i++;
	  }
	  postMap.put("videos", temp_video_map);
  }
  Widget unknown(){

  }*/

/*
  Widget expandingHTML(String html) {
    return new Padding(
      padding: new EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
      child: new ClipRect(
        child: new GestureDetector(
          onTap: () => setState(() => _textExpanded = !_textExpanded),
          child: new AnimatedSize(
            vsync: this,
            duration: const Duration(milliseconds: 120),
            alignment: Alignment.topCenter,
            child: new ConstrainedBox(
              constraints: _textExpanded ? new BoxConstraints(minHeight: 0.0) : new BoxConstraints(maxHeight: 200.0, minHeight: 0.0),
              child: new ListView(
                children: <Widget>[
                  new MarkdownBody(
                    data: html2md.convert(html),
                    styleSheet: new MarkdownStyleSheet.fromTheme(Theme.of(context)),
                  ),
                ],
                shrinkWrap: true,
                physics: new NeverScrollableScrollPhysics(),
              ),
            ),
          ),
        ),
      ),
    );
  }*/

}
